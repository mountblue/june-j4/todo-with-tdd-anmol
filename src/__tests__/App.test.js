import React from "react";
import ReactDOM from "react-dom";
import App from "../App";
import TodoInput from "../components/TodoInput";
import TodoList from "../components/TodoList";
import { shallow } from "enzyme";

describe("App Component", function() {
  it("App renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("should render subcomponents", () => {
    const wrapper = shallow(<App />);
    expect(
      wrapper.containsAllMatchingElements([<TodoInput />, <TodoList />])
    ).toEqual(true);
  });

  it("should start with an empty list", () => {
    const wrapper = shallow(<App />);
    const expectedResult = [];
    expect(wrapper.state("todos")).toEqual(expectedResult);
  });

  it("add items to the list", () => {
    const wrapper = shallow(<App />);
    wrapper.instance().addTask("new Task");
    const expectedResult = "new Task";
    expect(wrapper.state("todos")[0].task).toEqual(expectedResult);
  });

  it("pass addTask function to TodoInput compenent", () => {
    const wrapper = shallow(<App />);
    const todoInput = wrapper.find(TodoInput);
    const addTask = wrapper.instance().addTask;
    expect(todoInput.prop("onSubmit")).toEqual(addTask);
  });

  it("should pass if the props are bounded properly to the TodoInput component", () => {
    const wrapper = shallow(<App />);
    const todoInput = wrapper.find(TodoInput);
    todoInput.prop("onSubmit")("add new task");
    expect(wrapper.state("todos")[0].task).toEqual("add new task");
  });

  it("should strike on todo completed", () => {
    const wrapper = shallow(<App />);
    wrapper.instance().addTask("new Task");
    wrapper.instance().taskComplete(0);
    const expectedResult = true;
    expect(wrapper.state("todos")[0].status).toEqual(expectedResult);
  });

  it("pass taskComplete function to TodoList compenent", () => {
    const wrapper = shallow(<App />);
    const todoList = wrapper.find(TodoList);
    const taskComplete = wrapper.instance().taskComplete;
    expect(todoList.prop("taskChecked")).toEqual(taskComplete);
  });

  it("should delete the todo", () => {
    const wrapper = shallow(<App />);
    wrapper.instance().addTask("new Task");
    wrapper.instance().deleteTask(0);
    const expectedResult = [];
    expect(wrapper.state("todos")).toEqual(expectedResult);
  });

  it("pass deleteTask function to TodoList compenent", () => {
    const wrapper = shallow(<App />);
    const todoList = wrapper.find(TodoList);
    const deleteTask = wrapper.instance().deleteTask;
    expect(todoList.prop("taskDeleted")).toEqual(deleteTask);
  });
});
