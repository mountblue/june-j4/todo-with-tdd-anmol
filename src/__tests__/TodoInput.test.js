import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";
import TodoInput from "../components/TodoInput";
import { spy } from "sinon";

describe("TodoInput Component", () => {
  it("TodoInput renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<TodoInput />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("should contain an input and a button", () => {
    const wrapper = shallow(<TodoInput />);
    expect(wrapper.containsAllMatchingElements([<input />])).toEqual(true);
  });

  it("should call onSubmit when Enter is pressed", () => {
    const onKeyPress = spy();
    const wrapper = shallow(<TodoInput onSubmit={onKeyPress} />);
    const input = wrapper.find("input");
    input.simulate("keyPress", {
      key: "Enter",
      keyCode: 13,
      target: { value: "hello" }
    });
    expect(onKeyPress.calledOnce).toEqual(true);
  });
});
