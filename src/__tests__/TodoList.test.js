import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";
import TodoList from "../components/TodoList";
import { spy } from "sinon";

describe("TodoList Component", () => {
  it("TodoList renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<TodoList />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("Should render when list is empty", () => {
    const wrapper = shallow(<TodoList items={[]} />);
    const addList = wrapper.find("li");
    expect(addList).toHaveLength(0);
  });

  it("should render indefined items", () => {
    const wrapper = shallow(<TodoList items={undefined} />);
    const addList = wrapper.find("li");
    expect(addList).toHaveLength(0);
  });

  it("should render stored items", () => {
    const items = ["cook", "run", "dance"];
    const wrapper = shallow(<TodoList todos={items} />);
    const addList = wrapper.find("li");
    expect(addList).toHaveLength(3);
  });

  it("should call listClicked when list is clicked", () => {
    const onClick = spy();
    const item = ["cook"];
    const wrapper = shallow(<TodoList taskChecked={onClick} todos={item} />);
    const list = wrapper.find("li");
    list.simulate("click", {
      target: { value: "cook", parentNode: { parentNode: { children: "" } } }
    });
    expect(onClick.calledOnce).toEqual(true);
  });

  it("should call spanClicked when span is clicked", () => {
    const onClick = spy();
    const item = ["cook"];
    const wrapper = shallow(<TodoList taskDeleted={onClick} todos={item} />);
    const span = wrapper.find("span");
    span.simulate("click", {
      target: { value: "cook", parentNode: { parentNode: { children: "" } } }
    });
    expect(onClick.calledOnce).toEqual(true);
  });
});
