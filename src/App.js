import React, { Component } from "react";
import TodoInput from "./components/TodoInput";
import TodoList from "./components/TodoList";
import "./App.css";
const uuidv1 = require("uuid/v1");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: []
    };
    this.addTask = this.addTask.bind(this);
    this.taskComplete = this.taskComplete.bind(this);

    this.deleteTask = this.deleteTask.bind(this);
  }

  addTask(text) {
    // console.log(uuidv1);
    if (text !== "" && text.replace(/\s/g, "").length) {
      this.setState({
        todos: [].concat(this.state.todos).concat([
          {
            task: text,
            status: false,
            _id: uuidv1()
          }
        ])
      });
    }
  }
  taskComplete(index) {
    let newTodo = [...this.state.todos];
    if (newTodo[index].status === false) {
      newTodo[index].status = true;
    } else {
      newTodo[index].status = false;
    }
    this.setState({ todos: newTodo });
  }
  deleteTask(index) {
    let newTodo = [...this.state.todos];
    let itemId = newTodo[index]._id;

    newTodo = this.state.todos.filter(function(todo) {
      return todo._id !== itemId;
    });
    this.setState({ todos: newTodo });
  }
  render() {
    return (
      <div className="App">
        <p className="App-intro">todo list</p>
        <TodoInput onSubmit={this.addTask} />
        <TodoList
          todos={this.state.todos}
          taskChecked={this.taskComplete}
          taskDeleted={this.deleteTask}
        />
      </div>
    );
  }
}

export default App;
