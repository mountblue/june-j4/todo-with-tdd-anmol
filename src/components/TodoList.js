import React from "react";
import PropTypes from "prop-types";

export default function TodoList(props) {
  let listClicked = event => {
    let node = Array.prototype.slice.call(
      event.target.parentNode.parentNode.children
    );
    let checkNode = node.indexOf(event.target.parentNode);
    props.taskChecked(checkNode);
  };

  let spanClicked = event => {
    let node = Array.prototype.slice.call(
      event.target.parentNode.parentNode.children
    );
    let deleteNode = node.indexOf(event.target.parentNode);
    props.taskDeleted(deleteNode);
  };

  return props.todos ? (
    <ul>
      {props.todos.map(
        (todo, index) =>
          todo.status === false ? (
            <div className="listItemsDiv" key={index}>
              <li onClick={listClicked}>{todo.task}</li>
              <span onClick={spanClicked}>x</span>
            </div>
          ) : (
            <div className="listItemsDiv" key={index}>
              <li onClick={listClicked} className="lineThrough">
                {todo.task}
              </li>
              <span onClick={spanClicked}>x</span>
            </div>
          )
      )}
    </ul>
  ) : null;
}

TodoList.propTypes = {
  todos: PropTypes.array,
  taskChecked: PropTypes.func,
  taskDeleted: PropTypes.func
};
