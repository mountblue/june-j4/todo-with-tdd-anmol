import React from "react";
import PropTypes from "prop-types";

export default function TodoInput(props) {
  let handleEnter = event => {
    if (event.key === "Enter") {
      props.onSubmit(event.target.value);
      event.target.value = "";
    }
  };
  return (
    <div>
      <input
        className="taskInput"
        type="text"
        placeholder="add a new task"
        onKeyPress={handleEnter}
      />
    </div>
  );
}

TodoInput.propTypes = {
  onSubmit: PropTypes.func
};
